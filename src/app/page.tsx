import Image from "next/image";
import styles from "./page.module.css";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <h1>Home Page</h1>
      <br />
      <Link href="about">About Page</Link>
      <br />
      <br /> <Link href="contact">Contact Page</Link>
      <br />
      <br /> <Link href="users/Ahmed">User Ahmed</Link>
      <br />
      <br />
      <Link href="users/Unknown">User Unknown</Link>
    </>
  );
}
