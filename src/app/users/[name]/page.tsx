export default function UserPage(props: { params: { name: string } }) {
  return <h1>User: {props.params.name}</h1>;
}
